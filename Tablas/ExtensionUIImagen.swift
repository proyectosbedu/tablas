//
//  ExtensionUIImagen.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/28/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

extension UIImageView {
    func redondear (){
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
}


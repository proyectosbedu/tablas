//
//  ViewController.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/25/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tabla01: UITableView!
    
    var nombres:[String] = ["Javier", "Juan", "Omar", "Ricardo"]
    var coches:Array<Auto> = []
    
    var colors : [String:UIColor] = ["Blanco": UIColor.white, "Negro":
    UIColor.black, "Gris": UIColor.gray,"Turquesa":
    UIColor.cyan,"Rojo":
    UIColor.red,"Amarillo":UIColor.yellow,"Azul": UIColor.blue,
                "Verde": UIColor.green, "Naranja": UIColor.orange]
    
    let search = UISearchController(searchResultsController: nil)
    lazy var refreshcontrol:UIRefreshControl = {
        let refreshcontrol = UIRefreshControl()
        refreshcontrol.addTarget(self, action: #selector(actualizarq), for: UIControl.Event.valueChanged)
        return refreshcontrol
    }()
    
    @objc func actualizarq(){
        self.refreshcontrol.endRefreshing()
        let obj2:Auto = Auto(modelo: 2019, marca: "Nissan", color: "Naranja", placas: "wwe12", foto: "https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Ferrari-LaFerrari_2014_01.jpg?itok=d8foJLWc", tipo: "GT-R")
        coches.append(obj2)
        self.actualizarTabla()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabla01.delegate = self
        tabla01.dataSource = self
        
        let obj1:Auto = Auto(modelo: 2015, marca: "Tesla", color: "Gris", placas: "sxa662", foto: "https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Ferrari-LaFerrari_2014_01.jpg?itok=d8foJLWc", tipo: "Cybertruck")
        let obj2:Auto = Auto(modelo: 2018, marca: "Seat", color: "Rojo", placas: "xyq229", foto: "https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Ferrari-LaFerrari_2014_01.jpg?itok=d8foJLWc", tipo: "Ibiza")
        
        coches.append(obj1)
        coches.append(obj2)
        
        self.tabla01.addSubview(refreshcontrol)
    }
    
    //Numero de celdas
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coches.count
    }
    
    //Configurar una celda
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = UITableViewCell()
        cell.textLabel?.text = nombres[indexPath.row]
        return cell*/
        
        let obj:Auto = coches[indexPath.row]
        
        let cel = tableView.dequeueReusableCell(withIdentifier: "c", for: indexPath) as! AutoTableViewCell
        cel.lbTitulo.text = obj.marca
        cel.lbDescripcion.text = "\(obj.modelo)"
        //cel.backgroundColor = self.colors["\(obj.color)"]
        cel.backgroundColor = constantes.colores.rosa
        /*let imagen = UIImage(named: obj.foto)
        cel.imgAuto.image = imagen*/
        cel.lbtipo.text = obj.tipo
        //cel.imgAuto.redondear()
        
        let urlImg = obj.foto
        let urlReal:URL = URL(string: urlImg)!
        
        DispatchQueue.global(qos: .userInitiated).async {
            let imgData:NSData = NSData(contentsOf: urlReal)!
                
            DispatchQueue.main.async {
                let imagen = UIImage(data: imgData as Data)
                cel.imgAuto.image = imagen
                cel.imgAuto.redondear()
            }
        }
        
        return cel
    }
    
    @IBAction func actualizar(_ sender: Any) {
        let alert = UIAlertController(title: "Sistema", message: "¿Deseas agregar un nuevo campo?", preferredStyle: .alert)
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()
        alert.textFields![0].placeholder = "Modelo"
        alert.textFields![1].placeholder = "Marca"
        alert.textFields![2].placeholder = "Color"
        alert.textFields![3].placeholder = "Placas"
        alert.textFields![4].placeholder = "URL-Foto"
        alert.textFields![5].placeholder = "Tipo"
        alert.addAction(UIAlertAction(title: "OK", style: .default){ [unowned alert] _ in
            let obj1:Auto = Auto(modelo: Int(alert.textFields![0].text!)!, marca: alert.textFields![1].text!, color: alert.textFields![2].text!, placas: alert.textFields![3].text!, foto: alert.textFields![4].text!, tipo: alert.textFields![5].text!)
            print(obj1)
            self.coches.append(obj1)
            self.actualizarTabla()
        })
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    //Funcion para evento cancelar default
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            nombres.remove(at: indexPath.row)
            actualizarTabla()
        }
    }*/
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let accion01 = UITableViewRowAction(style: .normal, title: "Editar", handler: {(_, action) -> Void in
            let obj:Auto = self.coches[indexPath.row]
            let alert = UIAlertController(title: "Editar", message: "Seguro que deseas editar a \(self.coches[indexPath.row].marca) \(self.coches[indexPath.row].modelo)", preferredStyle: .alert)
            alert.addTextField()
            alert.addTextField()
            alert.addTextField()
            alert.addTextField()
            alert.addTextField()
            alert.addTextField()
            alert.textFields![0].placeholder = "Modelo"
            alert.textFields![1].placeholder = "Marca"
            alert.textFields![2].placeholder = "Color"
            alert.textFields![3].placeholder = "Placas"
            alert.textFields![4].placeholder = "Foto"
            alert.textFields![5].placeholder = "Tipo"
            alert.addAction(UIAlertAction(title: "Ok", style: .default) {[unowned alert] _ in
                let nuevoModelo = alert.textFields![0]
                let nuevaMarca = alert.textFields![1]
                let nuevoColor = alert.textFields![2]
                let nuevaPlaca = alert.textFields![3]
                let nuevaFoto = alert.textFields![4]
                let nuevoTipo = alert.textFields![5]
                //self.nombres.remove(at: indexPath.row)
                //self.nombres.append(nuevoNombre.text!)
                obj.modelo = Int(nuevoModelo.text!)!
                obj.marca = nuevaMarca.text!
                obj.color = nuevoColor.text!
                obj.placas = nuevaPlaca.text!
                obj.foto = nuevaFoto.text!
                obj.tipo = nuevoTipo.text!
                self.actualizarTabla()
            })
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        })
        accion01.backgroundColor = UIColor.orange
        
        let accion02 = UITableViewRowAction(style: .normal, title: "Eliminar", handler: {(_, action) -> Void in
            self.coches.remove(at: indexPath.row)
            self.actualizarTabla()
        })
        accion02.backgroundColor = UIColor.brown
        
        return[accion01, accion02]
    }
    
    
    
    func actualizarTabla() {
        DispatchQueue.main.async {
            self.tabla01.reloadData()
        }
    }
    
}


//
//  ExtensionBoton.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/28/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

extension UIButton {
    func redondear(){
        layer.cornerRadius = 12
        clipsToBounds = true
    }
    func colorb(){
        backgroundColor = constantes.colores.colorBoton
    }
}


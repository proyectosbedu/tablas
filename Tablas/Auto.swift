//
//  Auto.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/26/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation

class Auto {
    
    var modelo:Int
    var marca:String
    var color:String
    var placas:String
    var foto:String
    var tipo:String
    
    init(modelo:Int, marca:String, color:String, placas:String, foto:String, tipo:String) {
        self.modelo = modelo
        self.marca = marca
        self.color = color
        self.placas = placas
        self.foto = foto
        self.tipo = tipo
    }
    
    func arrancar() {
        
    }
    
    func frenar() {
        
    }
}

//
//  AutoTableViewCell.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/26/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

class AutoTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAuto: UIImageView!
    @IBOutlet weak var lbDescripcion: UILabel!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbtipo: UILabel!
    @IBOutlet weak var btn01: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btn01.redondear()
        btn01.colorb()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

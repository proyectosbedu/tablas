//
//  Constantes.swift
//  Tablas
//
//  Created by Javier Yllescas on 11/28/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import Foundation
import UIKit

struct constantes {
    
    struct colores {
        static let defColor = UIColor(red: 212, green: 390, blue: 202, alpha: 1)
        static let rosa = UIColor(red: 1, green: 39/255, blue: 144/255, alpha: 1)
        static let colorBoton = UIColor(red: 0, green: 90/255, blue: 100/255, alpha: 1)
    }
}
